#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>

#include "Gaussian.hpp"
#include "Exponential.hpp"
#include "Rejector.hpp"

#include "wc_ioutilities.hpp"

using namespace Eigen;
using namespace std;

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/gaussian/"
#define NSAMPLES 	1000
#define EXP_ALPHA 	0.96
#define REJ_REJECTION 	0.6

int main(void) {
	
	char igau[256];
	char ornd[256];
	char opp[256];
	char orpp[256];
	char oipp[256];

	sprintf(igau, "%s%sgaussian_classifier.dat", ROOTPATH, SUBPATH);
	sprintf(ornd, "%s%sgaussian_classifier_rnd.dat", ROOTPATH, SUBPATH);
	sprintf(opp,  "%s%sgaussian_classifier_pp.dat", ROOTPATH, SUBPATH);
	sprintf(orpp, "%s%sgaussian_classifier_rpp.dat", ROOTPATH, SUBPATH);
	sprintf(oipp, "%s%sgaussian_classifier_ipp.dat", ROOTPATH, SUBPATH);

	Gaussian gau;
	gau.Setup(igau);

	// Dump Gaussian classifier
	gau.Dump();
	
	// Initialize Exponential accumulator
	Exponential expon(EXP_ALPHA, gau.config.nclasses);
	
	// Initialize Rejector
	Rejector reject(REJ_REJECTION, gau.config.nclasses, Rejector::AsRecursive);

	MatrixXd RndF  = MatrixXd::Random(NSAMPLES, gau.config.nfeatures);
	VectorXd cFeat = VectorXd::Zero(gau.config.nfeatures);
	VectorXd cpp   = VectorXd::Zero(gau.config.nclasses);
	VectorXd crpp  = VectorXd::Zero(gau.config.nclasses);
	VectorXd cipp  = VectorXd::Zero(gau.config.nclasses);

	MatrixXd pp    = MatrixXd::Zero(NSAMPLES, gau.config.nclasses); 
	MatrixXd rpp   = MatrixXd::Zero(NSAMPLES, gau.config.nclasses); 
	MatrixXd ipp   = MatrixXd::Zero(NSAMPLES, gau.config.nclasses); 
	
	printf("[gaussian] - Testing classifier with alpha=%f and rejection=%f\n", EXP_ALPHA, REJ_REJECTION);
	for (unsigned int i = 0; i<1000; i++) {
		cFeat = RndF.row(i);
		
		gau.Run(cFeat, cpp);
	
		reject.Apply(cpp, crpp);
		
		expon.Apply(crpp, cipp);


		pp.row(i) = cpp;
		rpp.row(i) = crpp;
		ipp.row(i) = cipp;
	}
		
	printf("[gaussian] - Saving random data at: %s\n", ornd);
	wc_save(ornd, RndF, "data", "random");
	printf("[gaussian] - Saving raw posterior probabilities at: %s\n", opp);
	wc_save(opp,  pp,   "probabilities", "raw");
	printf("[gaussian] - Saving rejected posterior probabilities at: %s\n", orpp);
	wc_save(orpp, rpp,  "probabilities", "rejected");
	printf("[gaussian] - Saving integrated posterior probabilities at: %s\n", oipp);
	wc_save(oipp, ipp,  "probabilities", "integrated");
	
	return 0;

}
