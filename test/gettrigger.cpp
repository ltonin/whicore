#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>
#include <vector>
#include <xdfio.h>

#include "XDFReader.hpp"
#include "bcitk_utilities.hpp"

using namespace Eigen;
using namespace std;

#define ROOTPATH 	getenv("WHIDEBUG")
#define SUBPATH 	"extra/"
#define NSAMPLES 	1000

int main (int argc, char* argv[]) {

	int trigval = 1;

	if (argc < 2) 
		cout<<"[gettrigger] - Looking for trigger value 1 (default)"<<endl;
	else 
		trigval = atoi(argv[1]);

	
	
	char idata[256];
	
	MatrixXi tri;
	XDFReader xdf;
	vector<int> pos;
	std::vector<int>::iterator it;


	sprintf(idata, "%s%ssmr_online.gdf", ROOTPATH, SUBPATH);
	

	if(xdf.Open(idata) < 0)
		goto exit;	

	
	xdf.Import();
	xdf.Dump();

	xdf.GetTrigger(tri);

	tri.transposeInPlace();	

	bcitk_gettrigger(tri, pos, trigval);

	printf("[gettrigger] - Number of triggers with value %d: %ld\n", trigval, pos.size());;
	for (it = pos.begin(); it < pos.end(); it++) {
		printf("	    |- Position: %d\n", *it);
	}

	
exit:
	xdf.Close();
	return 0;


}

