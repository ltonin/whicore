#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>

#include "wc_ioutilities.hpp"
#include "Gaussian.hpp"

using namespace Eigen;
using namespace std;

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/gaussian/"

void dummy_classifier(gauconfig_t* config) {

	config->filename	= "/dummy/path/dummy/";
	config->subject 	= "b4";
	config->nclasses 	= 2;
	config->nprototypes 	= 4;
	config->nfeatures 	= 5;
	config->sharedcov	= 't';
	config->mimean  	= 0.02;
	config->micov  		= 0.04;

	uint32_t idchan[] = {7, 10, 12, 15, 10};
	config->idchan.assign(idchan, idchan + config->nfeatures);
	
	uint32_t idfreq[] = {12, 8, 10, 12, 14};
	config->idfreq.assign(idfreq, idfreq + config->nfeatures);

}

int main(void) {
	
	char ogau[256];
	sprintf(ogau, "%s%sgaussian_classifier_dummy.dat", ROOTPATH,  SUBPATH);

	Gaussian gau;
	gauconfig_t config;
	MatrixXd centers, covs;

	// Create dummy classifier
	printf("[gaussian_rwheader] - Create dummy gaussian classifier\n");
	dummy_classifier(&config);	

	centers = MatrixXd::Random(config.nclasses*config.nfeatures, config.nprototypes);
	covs    = MatrixXd::Random(config.nclasses*config.nfeatures, config.nprototypes);

	// Import classifier
	printf("[gaussian_rwheader] - Load dummy gaussian classifier from variables\n");
	gau.Setup(&config, centers, covs);

	// Dump dummy classifier
	printf("[gaussian_rwheader] - Dump imported dummy classifier\n");
	gau.Dump();
	
	// Save dummy classifier
	printf("[gaussian_rwheader] - Export dummy classifier at: %s\n", ogau);
	gau.Export(ogau);
	
	// Loading dummy classifier
	printf("[gaussian_rwheader] - Load dummy classifier from: %s\n", ogau);
	gau.Setup(ogau);

	// Dump dummy classifier
	printf("[gaussian_rwheader] - Dump loaded dummy classifier\n");
	gau.Dump();

	
	return 0;

}
