#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>

#include "Window.hpp"
#include "Hann.hpp"
#include "Pwelch.hpp"

#include "wc_ioutilities.hpp"

using namespace Eigen;
using namespace std;

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/pwelch/"
#define NSAMPLES 	100
#define NCHANNELS 	16

#define DATA_ROOT	"../extra/data/"

#define SIN_NSAMPLES	51200
#define SIN_NCHANNELS	16
#define SIN_FREQ 	10
#define SIN_SR 		512

#define PSD_WINSIZE 	256
#define PSD_NOVL 	128
#define PSD_NFFT 	(PSD_WINSIZE/2.0) + 1
#define PSD_DOLOG	0

void generate_sinwave_2d(Ref<MatrixXd> out){
	
	for (unsigned int i = 0; i<out.cols(); i++) 
		for(unsigned int t=0; t<out.rows(); t++)
			out(t, i) = sin((2.0 * M_PI * SIN_FREQ * (i + 1) * t) / (double)SIN_SR);
}

int main(void) {

	
	char isin[256];
	char opsd[256];
	char ogri[256];


	sprintf(isin, "%s%spwelch_sin.dat", ROOTPATH, SUBPATH);
	sprintf(opsd, "%s%spwelch_psd.dat",  ROOTPATH, SUBPATH);
	sprintf(ogri, "%s%spwelch_gri.dat",  ROOTPATH, SUBPATH);
	
	MatrixXd sinwave = MatrixXd::Zero(SIN_NSAMPLES, SIN_NCHANNELS);
	MatrixXd psd 	 = MatrixXd::Zero(PSD_NFFT, SIN_NCHANNELS);
	std::vector<unsigned int> grid;
	
	vector<unsigned int> idchan;
	vector<unsigned int> idfreq;
	

	// Generating sinwave
	generate_sinwave_2d(sinwave);
	
	printf("[pwelch] - Generating sinewave data:\n");
	printf("         | - Number of samples: 	%d\n", SIN_NSAMPLES);
	printf("         | - Number of channels: 	%d\n", SIN_NCHANNELS);
	printf("         | - Sampling rate: 		%d\n", SIN_SR);
	printf("         | - Original frequency: 	%d\n", SIN_FREQ);
	
	// Selecting specific features
	
	unsigned int idchan_a[] = {1, 2, 3, 4, 5};
	unsigned int idfreq_a[] = {2, 20, 30, 40, 54};

	idchan.assign(idchan_a, idchan_a + sizeof(idchan_a)/sizeof(unsigned int));
	idfreq.assign(idfreq_a, idfreq_a + sizeof(idfreq_a)/sizeof(unsigned int));

	VectorXd Features(idchan.size());
	
	// Initialize Pwelch
	Pwelch pwelch;
	pwelch.Setup(PSD_WINSIZE, Window::AsHamming, PSD_NOVL, SIN_SR, PSD_DOLOG);

	// Dump pwelch configuration
	pwelch.Dump();

	// Get psd grid
	grid = pwelch.config.grid;

	// Apply Pwelch
	printf("[pwelch] - Appling pwelch:\n");

	pwelch.Apply(sinwave);


	pwelch.Get(psd);
	pwelch.Get(Features, idchan, idfreq);

	printf("[pwelch] - Saving sinewave data at: %s\n", isin);
	wc_save(isin, sinwave, "data", "sinewave");
	printf("[pwelch] - Saving psd data at: %s\n", opsd);
	wc_save(opsd, psd, "data", "pwelch");
	printf("[pwelch] - Saving grid data at: %s\n", opsd);
	wc_save(ogri, grid, "data", "grid");

	return 0;
}
