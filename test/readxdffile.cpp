#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>
#include <xdfio.h>

#include "XDFReader.hpp"
#include "wc_ioutilities.hpp"

using namespace Eigen;
using namespace std;

#define NSAMPLE 100
#define XDFROOT "/mnt/data/Research/smr/20150309_a1/"
#define RESROOT "../extra/data/"

string xdffile = "a1.20150309.124227.online.mi.mi_bflh.gdf";

int main (void) {
	
	char idata[256];
	char fxdf[256];
	char oeeg[256];
	char oexg[256];
	char otri[256];
	
	MatrixXf eeg;
	MatrixXf exg;
	MatrixXi tri;

	sprintf(fxdf, "%s", xdffile.substr(0, xdffile.size() - 4).c_str());
	printf("[readxdffile] - XDF filename: %s\n", fxdf);

	sprintf(idata, "%s%s", XDFROOT, xdffile.c_str());
	sprintf(oeeg,  "%s%s_eeg.dat", RESROOT, fxdf);
	sprintf(oexg,  "%s%s_exg.dat", RESROOT, fxdf);
	sprintf(otri,  "%s%s_tri.dat", RESROOT, fxdf);
	
	XDFReader xdf;

	if(xdf.Open(idata) < 0)
		goto exit;	

	
	xdf.Import();

	xdf.Dump();
	xdf.DumpEvents();


	xdf.Get(eeg, XDFR_EEG);
	xdf.Get(exg, XDFR_EXG);
	xdf.GetTrigger(tri);

	printf("[readxdffile] - Saving eeg data at: %s\n", oeeg); 
	printf("[readxdffile] - Saving exg data at: %s\n", oexg); 
	printf("[readxdffile] - Saving tri data at: %s\n", otri); 
	wc_save(oeeg, eeg, "data", "eeg");
	wc_save(oexg, exg, "data", "exg");
	wc_save(otri, tri, "data", "tri");
	
exit:
	cout<<"Exit"<<endl;
	xdf.Close();
	return 0;


}

