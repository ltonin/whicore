#include <vector>
#include "XMLReader.hpp"

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/"

using namespace std;

int main(void) {

	XMLReader reader;
	int 	ivalue;
	float 	fvalue;
	string 	tvalue;
	string  avalue;

	int	ilvalue[4];
	float 	flvalue[4];
	vector<string> tlvalue;
	vector<string> alvalue;

	char xmlfile[256];
	sprintf(xmlfile, "%s%sxmlconfig.xml", ROOTPATH, SUBPATH);
	
	printf("[xmlreader] - Importing xml from: %s\n", xmlfile);

	reader.Import(xmlfile);

	const char* ipath = "Numbers/IntVal"; 
	const char* fpath = "Numbers/FloatVal";
	const char* tpath = "Text/StringVal";
	const char* apath = "Attribute/AttributeVal";
	const char* aattr = "param";
	const char* ilpath = "ListInt/IntVal";
	const char* flpath = "ListFloat/FloatVal";
	const char* tlpath = "ListString/StringVal";
	const char* alpath = "ListAttribute/AttributeVal";

	ivalue	= reader.GetInt(ipath);
	fvalue	= reader.GetFloat(fpath);
	tvalue	= reader.GetString(tpath);
	avalue	= reader.GetAttribute(apath, aattr);

	ilvalue[0] = reader.GetInt(ilpath, 1);
	ilvalue[1] = reader.GetInt(ilpath, 2);
	ilvalue[2] = reader.GetInt(ilpath, 3);
	ilvalue[3] = reader.GetInt(ilpath, 4);

	flvalue[0] = reader.GetFloat(flpath, 1);
	flvalue[1] = reader.GetFloat(flpath, 2);
	flvalue[2] = reader.GetFloat(flpath, 3);
	flvalue[3] = reader.GetFloat(flpath, 4);

	tlvalue.push_back(reader.GetString(tlpath, 1));
	tlvalue.push_back(reader.GetString(tlpath, 2));
	tlvalue.push_back(reader.GetString(tlpath, 3));
	tlvalue.push_back(reader.GetString(tlpath, 4));

	alvalue.push_back(reader.GetAttribute(alpath, aattr, 1));
	alvalue.push_back(reader.GetAttribute(alpath, aattr, 2));
	alvalue.push_back(reader.GetAttribute(alpath, aattr, 3));
	alvalue.push_back(reader.GetAttribute(alpath, aattr, 4));

	printf("[xmlconfig] + Check\n");
	printf("            | - Int value       [%s]: %d\n", ipath, ivalue);
	printf("            | - Float value     [%s]: %f\n", fpath, fvalue);
	printf("            | - Text  value     [%s]: %s\n", tpath, tvalue.c_str());
	printf("            | - Attribute value [%s (%s)]: %s\n", apath, aattr, avalue.c_str());
	printf("              + List Int        [%s]:\n", ilpath);
	printf("              | - IntVal   	(1): %d\n", ilvalue[0]);
	printf("              | - IntVal   	(2): %d\n", ilvalue[1]);
	printf("              | - IntVal   	(3): %d\n", ilvalue[2]);
	printf("              | - IntVal   	(4): %d\n", ilvalue[3]);
	printf("              + List Float      [%s]:\n", flpath);
	printf("              | - FloatVal 	(1): %f\n", flvalue[0]);
	printf("              | - FloatVal 	(2): %f\n", flvalue[1]);
	printf("              | - FloatVal 	(3): %f\n", flvalue[2]);
	printf("              | - FloatVal 	(4): %f\n", flvalue[3]);
	printf("              + List Text       [%s]:\n", tlpath);
	printf("              | - TextVal  	(1): %s\n", tlvalue.at(0).c_str());
	printf("              | - TextVal  	(2): %s\n", tlvalue.at(1).c_str());
	printf("              | - TextVal  	(3): %s\n", tlvalue.at(2).c_str());
	printf("              | - TextVal  	(4): %s\n", tlvalue.at(3).c_str());
	printf("              + List Attribute  [%s (%s)]:\n", alpath, aattr);
	printf("              | - AttributeVal  (1): %s\n", alvalue.at(0).c_str());
	printf("              | - AttributeVal  (2): %s\n", alvalue.at(1).c_str());
	printf("              | - AttributeVal  (3): %s\n", alvalue.at(2).c_str());
	printf("              | - AttributeVal  (4): %s\n", alvalue.at(3).c_str());

	return 0;


}
