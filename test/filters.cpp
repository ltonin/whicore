#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>

#include "Dc.hpp"
#include "Car.hpp"
#include "Laplacian.hpp"
#include "wc_ioutilities.hpp"

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/filters/"
#define NSAMPLES 	100
#define NCHANNELS 	16

using namespace Eigen;
using namespace std;

int main (void) {

	char irnd[256];
	char odc[256];
	char ocar[256];
	char olap[256];
	char imsk[256];

	sprintf(irnd, "%s%sfilters_rnd.dat", ROOTPATH, SUBPATH);
	sprintf(odc,  "%s%sfilters_dc.dat", ROOTPATH, SUBPATH);
	sprintf(ocar, "%s%sfilters_car.dat", ROOTPATH, SUBPATH);
	sprintf(olap, "%s%sfilters_lap.dat", ROOTPATH, SUBPATH);
	sprintf(imsk, "%sextra/lapmask_16ch.dat", ROOTPATH);
	
	printf("[filters] - Generating random raw data\n");
	MatrixXd rnd 	= MatrixXd::Random(NSAMPLES, NCHANNELS);
	MatrixXd dc 	= MatrixXd::Zero(NSAMPLES, NCHANNELS);
	MatrixXd car 	= MatrixXd::Zero(NSAMPLES, NCHANNELS);
	MatrixXd lap 	= MatrixXd::Zero(NSAMPLES, NCHANNELS);

	Car filt_car;
	Dc  filt_dc;
	
	printf("[filters] - Loading laplacian mask from: %s\n", imsk);
	Laplacian filt_lap;
	filt_lap.Setup(imsk);

	// Applying DC
	filt_dc.Apply(rnd, dc);
	
	// Applying CAR
	filt_car.Apply(rnd, car);

	// Applying Laplacian
	filt_lap.Apply(rnd, lap);
	
	printf("[filters] - Saving random raw data at: %s\n", irnd);
	wc_save(irnd, rnd, "data", "random");
	printf("[filters] - Saving dc filtered data at: %s\n", odc);
	wc_save(odc, dc, "data", "DC filter");
	printf("[filters] - Saving car filtered data at: %s\n", ocar);
	wc_save(ocar, car, "data", "CAR filter");
	printf("[filters] - Saving laplacian filtered data at: %s\n", olap);
	wc_save(olap, lap, "data", "LAP filter");

	return 0;
}
