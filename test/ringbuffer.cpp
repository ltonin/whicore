#include <iostream>
#include "RingBuffer.hpp"

using namespace Eigen;
using namespace std;


#define BUFF_NSAMPLES 	10
#define BUFF_NCHANNELS	2

#define FRAME_NSAMPLES	4
#define FRAME_NCHANNELS BUFF_NCHANNELS

int main(void) {



	RingBuffer rbuffer;
	rbuffer.Setup(BUFF_NSAMPLES, BUFF_NCHANNELS);

	MatrixXd Frame = MatrixXd::Random(FRAME_NSAMPLES, FRAME_NCHANNELS);
	
	for (unsigned int i = 1; i<10; i ++) {
		MatrixXd cframe;
		cframe = Frame * i;
		rbuffer.Add(cframe);
		rbuffer.Dump();
		cout << "=================" << endl;
		
		sleep(0.5);
		if (rbuffer.IsFull()) {
			cout<< "==============================================================" << endl;
			cout<< "                        BUFFER FULL                           " << endl;
			cout<< "==============================================================" << endl;
		}
	}
		

	return 0;

}
