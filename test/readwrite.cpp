#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>

#include "wc_ioutilities.hpp"

using namespace Eigen;
using namespace std;

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/readwrite/"
#define NSAMPLES 	100
#define NCHANNELS 	16

int main(void) {

	hdr_t ihdr;
	MatrixXd rnd = MatrixXd::Random(NSAMPLES, NCHANNELS);
	MatrixXd idat;

	char fdat[256];
	sprintf(fdat, "%s%sreadwrite_rnd.dat", ROOTPATH, SUBPATH);

	printf("[readwrite] - Saving random data (%d, %d) in WHITK format\n", (int)rnd.rows(), (int)rnd.cols());
	wc_save(fdat, rnd, "data", "random");

	printf("[readwrite] - Loading data, previous saved:\n");
	wc_load(fdat, idat, &ihdr);

	printf("[readwrite] - Data header: \n");
	printf(" 	    | - Format:   %s\n", ihdr.format.c_str());
	printf(" 	    | - Version:  %s\n", ihdr.version.c_str());
	printf(" 	    | - Type:     %s\n", ihdr.type.c_str());
	printf(" 	    | - Label:    %s\n", ihdr.label.c_str());
	printf("            | - Num rows: %d\n", (int)idat.rows());
	printf("            | - Num cols: %d\n", (int)idat.cols());

	return 0;
}

