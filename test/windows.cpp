#include <iostream>
#include <fstream>
#include <math.h>
#include <Eigen/Dense>

#include "Hann.hpp"
#include "Hamming.hpp"
#include "Flattop.hpp"
#include "Blackman.hpp"

#include "wc_ioutilities.hpp"

#define ROOTPATH 	getenv("WHICORE")
#define SUBPATH 	"extra/windows/"
#define NSAMPLES 	100
#define NCHANNELS 	16

using namespace Eigen;
using namespace std;

int main (void) {
	
	char irndm[256];
	char ihann[256];
	char ihamm[256];
	char ifltp[256];
	char iblkm[256];

	sprintf(irndm, "%s%swindows_rndm.dat", ROOTPATH,  SUBPATH);
	sprintf(ihann, "%s%swindows_hann.dat", ROOTPATH, SUBPATH);
	sprintf(ihamm, "%s%swindows_hamm.dat", ROOTPATH, SUBPATH);
	sprintf(ifltp, "%s%swindows_fltp.dat", ROOTPATH, SUBPATH);
	sprintf(iblkm, "%s%swindows_blkm.dat", ROOTPATH, SUBPATH);

	printf("[windows] - Generating random raw data\n");
	MatrixXd rndm 	= MatrixXd::Random(100, 16);
	MatrixXd hann 	= MatrixXd::Zero(100, 16);
	MatrixXd hamm 	= MatrixXd::Zero(100, 16);
	MatrixXd fltp	= MatrixXd::Zero(100, 16);
	MatrixXd blkm	= MatrixXd::Zero(100, 16);

	Hann 		wHann(100);
	Hamming 	wHamm(100);
	Flattop 	wFltp(100);
	Blackman 	wBlkm(100);

	// Applying Hann window
	wHann.Apply(rndm, hann);
	
	// Applying Hammin window
	wHamm.Apply(rndm, hamm);

	// Applying Flattop window
	wFltp.Apply(rndm, fltp);

	// Applying Blackman window
	wBlkm.Apply(rndm, blkm);

	printf("[windows] - Saving random raw data at: %s\n", irndm);
	wc_save(irndm, rndm, "data", "random");
	printf("[windows] - Saving hann windowed data at: %s\n", ihann);
	wc_save(ihann, hann, "data", "Hann window");
	printf("[windows] - Saving hamming windowed data at: %s\n", ihamm);
	wc_save(ihamm, hamm, "data", "Hamming window");
	printf("[windows] - Saving flattop windowed data at: %s\n", ifltp);
	wc_save(ifltp, fltp, "data", "Flattop window");
	printf("[windows] - Saving blackman windowed data at: %s\n", iblkm);
	wc_save(iblkm, blkm, "data", "Blackman window");

	return 0;
}
