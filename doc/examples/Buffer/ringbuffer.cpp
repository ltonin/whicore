#include <iostream>
#include <Eigen/Dense>
#include "RingBuffer.hpp"

using namespace Eigen;
using namespace std;

int main(void) {

	unsigned int bsize     = 10; 	// Buffer samples size
	unsigned int bchannels = 2;  	// Buffer channels size
	unsigned int fsamples  = 4; 	// Frame  sample size

	RingBuffer rbuffer(bsamples, bchannels);
	MatrixXd Frame  = MatrixXd::Random(fsamples, bchannels);
	MatrixXd cframe = MatrixXd::Zero(fsamples, bchannels);

	for (unsigned int i = 1; i<10; i ++) {
		cframe = Frame * i;
		rbuffer.Add(cframe);
		rbuffer.Dump();
		
		sleep(0.5);
		if (rbuffer.IsFull()) {
			cout<< "Buffer is full" << endl;
			break;
		}

	}	
	return 0;
}
