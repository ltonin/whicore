#ifndef WCEXCEPTION_HPP
#define WCEXCEPTION_HPP

#include <string>

class WcException {

	public:
		enum WcExceptionCode 
		{
			UNKNOWN_ERROR = 0,
			SETUP_ERROR,
			CLASSIFIER_TYPE_ERROR,
			MISMATCH_DATA_ERROR
		};

		WcException(unsigned int code = WcException::UNKNOWN_ERROR, std::string message = "Unknown error");
		~WcException(void);

		std::string What(void);
		unsigned int Code(void);
		void PrintError(void);
		void PrintError(std::string what, unsigned int code);

	private:
		unsigned int _code;
		std::string 	_message;


};
#endif
