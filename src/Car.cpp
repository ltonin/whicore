#ifndef CAR_CPP
#define CAR_CPP

#include "Car.hpp"

Car::Car(void) : Filter(Filter::inDomainSpatial) {}
Car::~Car(void) {}

void Car::Apply(const Eigen::Ref<const Eigen::MatrixXd>& in, Eigen::Ref<Eigen::MatrixXd> out) {
	out = in - (in.rowwise().mean()).replicate(1, in.cols());
}

#endif
