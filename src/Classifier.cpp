#ifndef CLASSIFIER_CPP
#define CLASSIFIER_CPP

#include "Classifier.hpp"

Classifier::Classifier(int type) {
	this->_type = type;
}

Classifier::~Classifier(void) {}

int Classifier::GetType(void) {
	return this->_type;
}

void Classifier::SetName(std::string name) {
	this->_name = name;
}

std::string Classifier::GetName(void) {
	return this->_name;
}

#endif
