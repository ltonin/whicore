#ifndef DC_CPP
#define DC_CPP

#include "Dc.hpp"

Dc::Dc(void) : Filter(Filter::inDomainTime) {}

Dc::~Dc(void) {}

void Dc::Apply(const Eigen::Ref<const Eigen::MatrixXd>& in, Eigen::Ref<Eigen::MatrixXd> out) {
	out = in - (in.colwise().mean()).replicate(in.rows(), 1);
}


#endif
