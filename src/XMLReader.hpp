#ifndef XMLREADER_HPP
#define XMLREADER_HPP

#include <string>
#include <tinyxml2.h>

class XMLReader {

	public:
		XMLReader(void);
		~XMLReader(void);

		virtual int Import(const char* xmlfile);

		int GetInt(const char* ppath, unsigned int idelem = 1);
		bool GetBool(const char* ppath, unsigned int idelem = 1);
		float GetFloat(const char* ppath,  unsigned int idelem = 1);
		std::string GetString(const char* ppath,  unsigned int idelem = 1);
		std::string GetAttribute(const char* ppath, const char* attr, unsigned int idelem = 1);

	protected:
		int MoveTo(const char* ppath, unsigned int idelem);

		// TO IMPLEMENT
		bool CheckXml(void) { return false; };


	protected:
		tinyxml2::XMLDocument 	_xmlfile;
		tinyxml2::XMLElement*	_pElement;		

};

#endif
