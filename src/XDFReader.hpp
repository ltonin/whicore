#ifndef XDFREADER_HPP
#define XDFREADER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <xdfio.h>

#define NSAMPLE 100


struct xdfevent {
	int		evtcod;
	unsigned int 	evtpos;
	unsigned int	evtdur;
};

static const char * XDFformat[] = {
	"XDF_ANY",
	"XDF_EDF",
	"XDF_EDFP",
	"XDF_BDF",
	"XDF_GDF1",
	"XDF_GDF2"
};

enum XDFR_T  {
	XDFR_EEG,
	XDFR_EXG
};

class XDFReader {

	public:
		XDFReader(void);
		~XDFReader(void);

		int Import(void);
		int Open(const char* filname);
		int Close(void);
		
		template <typename Derived>
		void Get(Eigen::MatrixBase<Derived> const & out_, XDFR_T typ); 
		
		template <typename Derived>
		void GetTrigger(Eigen::MatrixBase<Derived> const & out_); 
	
		std::vector<xdfevent> GetEvents(void);
		
		std::string GetFilename(void);
		std::string GetFormat(void);
		int GetNumCh(void);
		int GetNumEEG(void);
		int GetNumEXG(void);
		int GetNumTRI(void);
		int GetFrequency(void);
		int GetNumEvt(void);
		int GetNumEvtType(void);

		int GetNumSamples(void);

		void Dump(void);
		void DumpEvents(void);

	private:
		int initialize(void);
		int getformat(void);
		int getnchannels(void);
		int getnevents(void);
		int getsamplefreq(void);
		int readheader(void);
		int readevents(void);
		int readeventGDF(void);
		int readeventBDF(void);

		void handle_error(int nerr, std::string msg = "");
		void handle_error(std::string msg);

	private:
		struct xdf *_xdf;
		struct xdfch *_xdfch;

		// Header information
		std::string _filename;
		std::string _flabel;
		xdffiletype _format;
		int _freq;
		int _nch;
		int _neeg;
		int _nexg;
		int _ntri;
		int _nevt;
		int _nevttype;
		
		// XDF data and events
		Eigen::MatrixXf _eeg;
		Eigen::MatrixXf _exg;
		Eigen::MatrixXi _tri;
		std::vector<xdfevent> _xdfevt;
	
	
		unsigned int _nsample;
		size_t * _strides;

	public: 
		/*! \brief Eigen flag to keep the memory block of the matrix
		 * aligned
		 */
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

template <typename Derived>
void XDFReader::Get(Eigen::MatrixBase<Derived> const & out_, XDFR_T typ) {

	Eigen::MatrixBase<Derived>& out = const_cast< Eigen::MatrixBase<Derived>& >(out_);

	switch(typ) {
		case XDFR_EEG:
			out.derived().resize(this->_neeg, this->_nsample);
			out = this->_eeg;
			break;
		case XDFR_EXG:
			out.derived().resize(this->_nexg, this->_nsample);
			out = this->_exg;
			break;
		default:
			std::cout<<"error"<<std::endl;
	}
		
}

template <typename Derived>
void XDFReader::GetTrigger(Eigen::MatrixBase<Derived> const & out_) {

	Eigen::MatrixBase<Derived>& out = const_cast< Eigen::MatrixBase<Derived>& >(out_);

	out.derived().resize(this->_ntri, this->_nsample);
	out = this->_tri;
		
}
#endif
