#ifndef QDA_CPP
#define QDA_CPP

#include "Qda.hpp"


Qda::Qda(unsigned int nclasses, unsigned int nfeatures) : Classifier(Classifier::AsQDA) {}

Qda::~Qda(void) {}

void Qda::Load(std::string filename) {}

void Qda::Run(const Ref<const VectorXd>& features, Ref<Vector2d> pp) {}

#endif
