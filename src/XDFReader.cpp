#ifndef XDFREADER_CPP
#define XDFREADER_CPP


#include "XDFReader.hpp"

XDFReader::XDFReader(void) {
	this->_filename = "";
	this->_xdf 	= NULL;
	this->_xdfch 	= NULL;
	this->_neeg 	= -1;
	this->_nexg 	= -1;
	this->_ntri 	= -1;
	this->_strides  = NULL;
	this->_nsample  = 0;
	this->_freq 	= -1;
	this->_nevt 	= -1;
	this->_nevttype = -1;
}

XDFReader::~XDFReader(void) {
}

int XDFReader::Open(const char* filename) {

	int retcode = 0;

	this->_filename = filename;
	this->_xdf = xdf_open(filename, XDF_READ, XDF_ANY);

	if (this->_xdf == NULL) { 
		handle_error(errno, filename);
		retcode = -1;
	} else {
		retcode = this->readheader(); 
		retcode = this->initialize();
	}

	return retcode;
}

int XDFReader::Close(void) {
	int retcode;

	if(retcode = xdf_close(this->_xdf) < 0)
		handle_error(errno);

	return retcode;
}

std::vector<xdfevent> XDFReader::GetEvents(void) {
	return this->_xdfevt;
}


std::string XDFReader::GetFilename(void) {
	return this->_filename;
}

std::string XDFReader::GetFormat(void) {
	return this->_flabel;
}

int XDFReader::GetNumCh(void) {
	return this->_nch;
}

int XDFReader::GetNumEEG(void) {
	return this->_neeg;
}

int XDFReader::GetNumEXG(void) {
	return this->_nexg;
}

int XDFReader::GetNumTRI(void) {
	return this->_ntri;
}

int XDFReader::GetFrequency(void) {
	return this->_freq;
}

int XDFReader::GetNumEvt(void) {
	return this->_nevt;
}

int XDFReader::GetNumEvtType(void) {
	return this->_nevttype;
}

int XDFReader::GetNumSamples(void) {
	return (int)this->_nsample;
}

int XDFReader::Import(void) {

	float* eeg = NULL; 
	float* exg = NULL;
	int32_t* tri = NULL;

	Eigen::MatrixXf eegbuff(this->_neeg, NSAMPLE);
	Eigen::MatrixXf exgbuff(this->_nexg, NSAMPLE);
	Eigen::MatrixXi tribuff(this->_ntri, NSAMPLE);

	ssize_t ns;
	size_t nstot = 0;
	int retcode = 0;

	
	// Define arrays and prepare xdf transfer
	if (xdf_define_arrays(this->_xdf, 3, this->_strides)
	    || xdf_prepare_transfer(this->_xdf)) {
		handle_error(errno);
		retcode = -1;
	}

	// Alloc memory for buffers
	eeg = (float*)calloc(NSAMPLE*this->_neeg,sizeof(float*));
	exg = (float*)calloc(NSAMPLE*this->_nexg,sizeof(float*));
	tri = (int32_t*)calloc(NSAMPLE*this->_ntri,sizeof(int32_t*));

	
	// Start reading the file
	while(1) {

		ns = xdf_read(this->_xdf, NSAMPLE, eeg, exg, tri);
		
		if (ns < 0) { 
			handle_error(errno);
			retcode = -1;
			break;
		}

		nstot += ns;
		retcode += ns;

		if (ns != NSAMPLE) {
			eegbuff.resize(Eigen::NoChange, ns);
			exgbuff.resize(Eigen::NoChange, ns);
			tribuff.resize(Eigen::NoChange, ns);
		}

		eegbuff = Eigen::Map<Eigen::MatrixXf>(eeg, eegbuff.rows(), eegbuff.cols());
		exgbuff = Eigen::Map<Eigen::MatrixXf>(exg, exgbuff.rows(), exgbuff.cols());
		tribuff = Eigen::Map<Eigen::MatrixXi>((int32_t*)tri, tribuff.rows(), tribuff.cols());
		
		this->_eeg.conservativeResize(Eigen::NoChange, nstot);	
		this->_exg.conservativeResize(Eigen::NoChange, nstot);	
		this->_tri.conservativeResize(Eigen::NoChange, nstot);	

		this->_eeg.rightCols(ns) = eegbuff;
		this->_exg.rightCols(ns) = exgbuff;
		this->_tri.rightCols(ns) = tribuff;

		if (ns == 0)
			break;
	}


	this->_nsample = nstot;

	// Reading events table
	this->readevents();


	free(eeg);
	free(exg);
	free(tri);


	return retcode;

}

void XDFReader::Dump(void) {

	printf("[XDFReader] XDF info:\n");
	printf(" |+  Capabilities:\n");
	printf("  |- File:		%s\n", this->GetFilename().c_str());
	printf("  |- Format:		%s\n", this->GetFormat().c_str());
	printf(" |+ Samples:		%d\n", this->GetNumSamples());
	printf(" |+ Channels:		%d\n", this->GetNumCh());
	printf("  |- EEG:		%d\n", this->GetNumEEG());
	printf("  |- EXG:		%d\n", this->GetNumEXG());
	printf("  |- TRI:		%d\n", this->GetNumTRI());
	printf(" |- Frequency:		%d Hz\n", this->GetFrequency());
	printf(" |- N events:		%d\n", this->GetNumEvt());
	printf(" |- N events type:	%d\n", this->GetNumEvtType());
	printf("\n");
}

void XDFReader::DumpEvents(void) {

	unsigned int i = 0;
	std::vector<xdfevent>::iterator it;
	printf("[XDFReader] Events table\n");
	printf("	   | Code | Pos | Dur |\n"); 
	
	for (it = this->_xdfevt.begin(); it < this->_xdfevt.end(); it++) {
		printf("|- Event %d: %d\t%d\t%d\n", i, (*it).evtcod, (*it).evtpos, (*it).evtdur);
		i++;
	}
		
		
		//for(i=0; i<this->_nevt; i++)
		//printf("|- Event %d: %d\t%d\t%d\n", i, this->_xdfevt[i].evtcod, this->_xdfevt[i].evtpos, this->_xdfevt[i].evtdur);

}


/////////////////////////// HEADER READER ////////////////////////////////////
int XDFReader::readheader(void) {

	int retcode;

	if(retcode = this->getformat()< 0)
	   	handle_error("[XDFReader] Error getting format information");

	if(retcode = this->getnchannels()< 0)
	   	handle_error("[XDFReader] Error getting channels information");

	if(retcode = this->getsamplefreq()< 0)
	   	handle_error("[XDFReader] Error getting frequency information");

	if(retcode = this->getnevents()< 0)
		handle_error("[XDFReader] Error getting events information");
	
	return retcode;
}

int XDFReader::getformat(void) {
	
	int retcode;
	
	if(retcode = xdf_get_conf(this->_xdf, XDF_F_FILEFMT, &this->_format, XDF_NOF) < 0)
		handle_error(errno);

	this->_flabel = XDFformat[this->_format];
	return retcode;
}

int XDFReader::getnchannels(void) {

	int retcode;
	
	if(retcode = xdf_get_conf(this->_xdf, XDF_F_NCHANNEL, &this->_nch, XDF_NOF) < 0)
		handle_error(errno);

	switch(this->_format) {
		case XDF_BDF:
			this->_nexg = 24;
			this->_ntri = 1;
			break;
		case XDF_GDF1:
			this->_nexg = 0;
			this->_ntri = 1;
			break;
		case XDF_GDF2:
			this->_nexg = 0;
			this->_ntri = 1;
			break;
		default:
			handle_error("Not recognize xdf format (only BDF, GDF1 and GDF2)");
			retcode = -1;
			break;
	}

	this->_neeg = this->_nch - this->_nexg - this->_ntri;

	return retcode;
}

int XDFReader::getsamplefreq(void) {
	int retcode;
	
	if(retcode = xdf_get_conf(this->_xdf, XDF_F_SAMPLING_FREQ, &this->_freq, XDF_NOF) < 0)
		handle_error(errno);

	return retcode;
}

int XDFReader::getnevents(void) {

	int retcode;

	return retcode;
}
/////////////////////////// HEADER READER ////////////////////////////////////

/////////////////////// BUFFER INITIALIZATION ////////////////////////////////

int XDFReader::initialize(void) {
	
	int retcode = 0;
	int offset;
	unsigned int i;

	this->_strides = (size_t*)malloc(3*sizeof(size_t));

	for (i=0; i<this->_nch; i++) {
		this->_xdfch = xdf_get_channel(this->_xdf, i);
			
		if(this->_xdfch == NULL) {
			handle_error(errno);
			retcode = -1;
		}

		if(retcode = xdf_set_chconf(this->_xdfch, XDF_CF_ARRINDEX, -1, XDF_NOF) < 0)
			handle_error(errno);
	}
	
	// Configure the channels requested
	offset = 0;
	for (i=0; i<this->_neeg; i++) {
		if(retcode = xdf_set_chconf(xdf_get_channel(this->_xdf, i), 
						XDF_CF_ARRDIGITAL, 0,
						XDF_CF_ARRINDEX, 0,
						XDF_CF_ARRTYPE, XDFFLOAT,
						XDF_CF_ARROFFSET, offset, 
						XDF_NOF) < 0) 
			handle_error(errno);

			
		offset += sizeof(float);
	}
	this->_strides[0] = offset;
	
	offset = 0;
	for (i=0; i<this->_nexg; i++) {
		if(retcode = xdf_set_chconf(xdf_get_channel(this->_xdf, i+this->_neeg),
						XDF_CF_ARRDIGITAL, 0,
						XDF_CF_ARRINDEX, 1,
						XDF_CF_ARRTYPE, XDFFLOAT,
						XDF_CF_ARROFFSET, offset,
						XDF_NOF) < 0)
			handle_error(errno);


		offset += sizeof(float);
	}
	this->_strides[1] = offset;

	offset = 0;
	for (i=0; i<this->_ntri; i++) {
		if(retcode = xdf_set_chconf(xdf_get_channel(this->_xdf, i+this->_neeg+this->_nexg),
						XDF_CF_ARRDIGITAL, 0,
						XDF_CF_ARRINDEX, 2,
						XDF_CF_ARRTYPE, XDFINT32,
						XDF_CF_ARROFFSET, offset,
						XDF_NOF) < 0)
			handle_error(errno);
		
		offset += sizeof(int32_t);
	}
	this->_strides[2] = offset;


	this->_eeg.resize(this->_neeg, NSAMPLE);
	this->_exg.resize(this->_nexg, NSAMPLE);
	this->_tri.resize(this->_ntri, NSAMPLE);


	return retcode;
}
/////////////////////// BUFFER INITIALIZATION ////////////////////////////////


/////////////////////////// EVENT READER ////////////////////////////////////
int XDFReader::readeventGDF(void) {
	
	int nevtT, nevt, i, freq;
	int nevttype;
	unsigned int evttyp;
	const char* evtdesc[256];
	
	int* 	evtcodT;
	double* evtonsT;
	double* evtdurT;

	xdfevent evt;

	xdf_get_conf(this->_xdf, XDF_F_NEVENT, &nevtT, 
					   XDF_F_NEVTTYPE, &nevttype, XDF_NOF);

	nevt  = nevtT/2;
	freq  = this->_freq;

	evtcodT = new int[nevtT];
	evtonsT = new double[nevtT];
	evtdurT = new double[nevtT];

	for (i=0; i<nevtT; i++) {
		xdf_get_event(this->_xdf, i, &evttyp, &evtonsT[i], &evtdurT[i]);
		xdf_get_evttype(this->_xdf, evttyp, &evtcodT[i], evtdesc);
	}
	
	for(i=0; i<nevt; i++) {
		evt.evtcod  = evtcodT[2*i];
		evt.evtpos  = (unsigned int)round(evtonsT[2*i]*freq);	
		evt.evtdur  = (unsigned int)round((evtonsT[2*i+1]-evtonsT[2*i])*freq);

		this->_xdfevt.push_back(evt);
	}

	this->_nevt 	= nevt;
	this->_nevttype = nevttype;

	free(evtcodT);
	free(evtonsT);
	free(evtdurT);
}

int XDFReader::readeventBDF(void) {

	unsigned int i;
	unsigned int cevt, pevt;
	int slope;
	xdfevent evt;
	std::vector<int> evttype;

	for(i=1; i<this->_nsample; i++) {
		pevt = 255 & this->_tri(i-1);	
		cevt = 255 & this->_tri(i);
		
		slope = cevt - pevt;
		
		if (slope > 0) {			// Raising trigger edge
			evt.evtcod = cevt;
			evt.evtpos = i;
		} else if (slope < 0) {			// Falling trigger edge
			evt.evtdur = i - evt.evtpos;
			this->_xdfevt.push_back(evt);
			
			evt.evtcod = 0;			// Reset event structure
			evt.evtpos = 0;
			evt.evtpos = 0;
		}
	}

	this->_nevt = this->_xdfevt.size();

	for (std::vector<xdfevent>::iterator it = this->_xdfevt.begin(); it < this->_xdfevt.end(); it++) {

		if(find(evttype.begin(), evttype.end(), (*it).evtcod) == evttype.end())
                	evttype.push_back((*it).evtcod);
	}
	this->_nevttype = evttype.size();
}


int XDFReader::readevents(void) {

	switch(this->_format) {
		case XDF_BDF:
			this->readeventBDF();
			break;
		case XDF_GDF2:
			this->readeventGDF();
			break;
	}

}

/////////////////////////// EVENT READER ////////////////////////////////////



void XDFReader::handle_error(int nerr, std::string msg) {

	std::string objname = "XDFReader";

	if (msg.empty() == false)
		msg = ": " + msg;
	
	fprintf(stderr, "[%s] - (%i) %s%s\n", objname.c_str(), nerr, strerror(nerr), msg.c_str()); 
}

void XDFReader::handle_error(std::string msg) {

	std::string objname = "XDFReader";
	fprintf(stderr, "[%s] - %s\n", objname.c_str(), msg.c_str()); 
}

#endif
