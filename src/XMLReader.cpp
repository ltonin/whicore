#ifndef CONFIGURE_CPP
#define CONFIGURE_CPP

#include "XMLReader.hpp"

XMLReader::XMLReader(void) {
	this->_pElement = NULL;
}

XMLReader::~XMLReader(void) {
	//free(this->_pElement);
}

int XMLReader::Import(const char* xmlfile) {
	int retcod;
	retcod = this->_xmlfile.LoadFile(xmlfile);
	return retcod;
}


int XMLReader::GetInt(const char* ppath, unsigned int idelem) {

	int value;

	this->MoveTo(ppath, idelem);
	this->_pElement->QueryIntText(&value);

	return value;
}

bool XMLReader::GetBool(const char* ppath, unsigned int idelem) {
	return (bool)this->GetInt(ppath, idelem);
}

float XMLReader::GetFloat(const char* ppath, unsigned int idelem) {

	float value;

	this->MoveTo(ppath, idelem);
	this->_pElement->QueryFloatText(&value);

	return value;
}

std::string XMLReader::GetString(const char* ppath, unsigned int idelem) {

	this->MoveTo(ppath, idelem);
	std::string text(this->_pElement->GetText());

	return text;
}

std::string XMLReader::GetAttribute(const char* ppath, const char* attr, unsigned int idelem) {

	std::string text;

	this->MoveTo(ppath, idelem);

	return this->_pElement->Attribute(attr);
}

int XMLReader::MoveTo(const char* ppath, unsigned int idelem) {

	char* pname;
	char  nname[256];
	char  spath[256];
	int   retcod = 0;
	int value;
	unsigned int i;

	tinyxml2::XMLElement *cElement;
	sprintf(spath, "%s", ppath);

	this->_pElement = this->_xmlfile.FirstChild()->ToElement();

	if(this->_pElement == NULL) {
		printf("[XMLReader] - Error on reading XML file\n");
		return -1;
	}

	pname = strtok(spath, "/");

	while(pname != NULL) {
		memset(&nname[0], 0, sizeof(nname));
		sprintf(nname, "%s", pname);

		cElement = this->_pElement->FirstChildElement(nname); 		
		if(this->_pElement == NULL) { 
			printf("[XMLReader] - Error parsing. Node %s not found\n", nname);
			retcod = -1;
			break;
		}
		
		this->_pElement = cElement;
		cElement = NULL;
		pname = strtok(NULL, "/");
	}


	for(i=0; i<idelem - 1; i++) {
		cElement = this->_pElement->NextSiblingElement(this->_pElement->Value());
		this->_pElement = cElement;

		if(this->_pElement == NULL) {
			printf("[XMLReader] - Error looking for siblings. Sibling %d not found\n", i+1);
			retcod = -1;
			break;
		}
			
	}

	return retcod;
}


#endif
