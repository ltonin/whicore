#ifndef WC_IOUTILITIES_CPP
#define WC_IOUTILITIES_CPP

#include "wc_ioutilities.hpp"




std::string wc_readstring(std::ifstream& ifs) {

	// read size
	byte_t len = wc_readbyte(ifs);

	char* buffer = new char[len];
	ifs.read(buffer, len);

	std::string str(buffer, len);
	delete[] buffer;

	return str;
}

byte_t wc_writestring(std::ofstream& ofs, std::string str) {

	byte_t len = (byte_t) str.length();

	// write size
	wc_writebyte(ofs, len);
	ofs.write(str.c_str(), len);

	return len;
}

void wc_readheader(std::ifstream& ifs, hdr_t* hdr) {
	
	hdr->format 	= wc_readstring(ifs);
	hdr->version 	= wc_readstring(ifs);
	hdr->type 	= wc_readstring(ifs);
	hdr->label 	= wc_readstring(ifs);
}

void wc_writeheader(std::ofstream& ofs, std::string type, std::string label) {

	wc_writestring(ofs, WHITK_FORMAT);
	wc_writestring(ofs, WHITK_VERSION);
	wc_writestring(ofs, type);
	wc_writestring(ofs, label);
}


void wc_save(const char* filename, std::string in, std::string type, std::string label) {
	
	std::ofstream ofs;
	std::string sfile (filename);

	ofs.exceptions (std::ifstream::failbit | std::ifstream::badbit);
	
	try {
		ofs.open(filename, std::ios::binary);
		wc_writeheader(ofs, type, label);
		wc_writestring(ofs, in);
		ofs.close();
	} catch (std::ofstream::failure e) {
		printf("[whicore] - Can't save the file at: %s (%s)\n", filename, e.what());
		exit(0);
	}
}

template<class Derived>
std::string wc_load(const char* filename, hdr_t* hdr) {

	std::string out;
	std::ifstream ifs;

	ifs.exceptions (std::ifstream::failbit | std::ifstream::badbit);
	
	try {
		ifs.open(filename, std::ios::in | std::ios::binary);
		wc_readheader(ifs, hdr);	
		out = wc_readstring(ifs);
		ifs.close();
	} catch (std::ifstream::failure e) {
		printf("[whicore] - Can't read the file from: %s (%s)\n", filename, e.what());
		exit(0);
	}

	return out;
}



































bool wc_check_string(std::string par, std::string par2, std::string message) {
	if(par.compare(par2) != 0)
		throw WcException(WcException::MISMATCH_DATA_ERROR, message);
}

#endif
