#ifndef ALGORITHM_CPP
#define ALGORITHM_CPP

#include "Algorithm.hpp"

Algorithm::Algorithm(int type)  {
	this->_type = type;
}

Algorithm::~Algorithm(void) {
}

int Algorithm::GetType(void) {
	return this->_type;
}

void Algorithm::SetType(int type) {
	this->_type = type;
}

std::string Algorithm::GetName(void) {
	return this->_name;
}

void Algorithm::SetName(std::string name) {
	this->_name = name;
}
#endif
