#ifndef BCITK_UTILITIES_HPP
#define BCITK_UTILITIES_HPP

#include <iostream>
#include <iomanip> 
#include <fstream>
#include <math.h>
#include <vector>
#include <Eigen/Dense>
#include <stdint.h>
#include "Device.hpp"

using namespace Eigen;
//using namespace std;

//template <typename Derived>
//void bcitk_save(const MatrixBase<Derived>& in, const char* filename, ios::openmode mode = std::ios::binary) {
//	
//	unsigned int size = in.size();
//	typedef Ref<const Matrix<typename Derived::Scalar,  Derived::RowsAtCompileTime, Derived::ColsAtCompileTime> > RefT;
//	RefT in_ref(in);
//	
//	std::ofstream ofs;	
//	ofs.exceptions (std::ifstream::failbit | std::ifstream::badbit);
//	
//	try {
//		ofs.open(filename, std::ios::binary | mode);
//		ofs.write(reinterpret_cast<const char*>(in_ref.data()), sizeof(typename Derived::Scalar)*size);
//		ofs.close();
//	} catch (std::ifstream::failure e) {
//		printf("[bcitk] - Can't save the file: %s (%s)\n", filename, e.what());
//		//std::cout<<"[Error] - Can't save the file: " << filename <<" (" << e.what() << ")" <<std::endl;
//		exit(0);
//	}
//
//}
//
//template <typename Derived>
//void bcitk_load(MatrixBase<Derived>& out, const char* filename) {
//
//	unsigned int size = out.size();
//	typename Derived::Scalar * buffer;
//	buffer = (typename Derived::Scalar *)malloc(sizeof(typename Derived::Scalar)*size);
//	
//	std::ifstream ifs;
//	ifs.exceptions (std::ifstream::failbit | std::ifstream::badbit);
//	try {
//		ifs.open(filename, std::ios::in | std::ios::binary);
//		ifs.read(reinterpret_cast<char*>(buffer), sizeof(typename Derived::Scalar)*size);
//		ifs.close();
//	} catch (std::ifstream::failure e) {
//		printf("[bcitk] - Can't read the file: %s (%s)\n", filename, e.what());
//		//std::cout<<"[Error] - Can't read the file: " << filename <<" (" << e.what() << ")" <<std::endl;
//		exit(0);
//	}
//
//	
//
//	out = Map<MatrixXd>(buffer, out.rows(), out.cols());
//
//	free(buffer);
//}


static inline void bcitk_progressbar(unsigned int x, unsigned int n, unsigned int w = 50)
{
    if ( (x != n) && (x % (n/100+1) != 0) ) return;
 
    float ratio  =  x/(float)n;
    int   c      =  ratio * w;
 
    std::cout << setw(3) << (int)(ratio*100 + 1) << "% [";
    for (int x=0; x<c; x++) std::cout << "=";
    for (int x=c; x<w; x++) std::cout << " ";
    std::cout << "]\r" << std::flush;
}


template <typename Derived>
int bcitk_gettrigger(const MatrixBase<Derived>& in, std::vector<int>& pos, int value = 1)
{
	typedef typename Derived::Scalar Scalar;
	
	unsigned int i;
	int nre = 0;

	const Scalar nsamples = static_cast<Scalar>(in.rows());
	int cvalue;
	int pvalue;

	for (i = 1; i < nsamples; i++) {
		cvalue = static_cast<Scalar>(in(i));
		pvalue = static_cast<Scalar>(in(i-1));
		if(((cvalue - pvalue) > 0) & (cvalue == value)) { 	// raising edge and value
			pos.push_back(i);	
			nre++;
		}
	}

	return nre;
}


#endif
