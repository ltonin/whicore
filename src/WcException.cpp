#ifndef WCEXCEPTION_CPP
#define WCEXCEPTION_CPP

#include "WcException.hpp"


WcException::WcException(unsigned int code, std::string message) {
	this->_code 	= code;
	this->_message 	= message;
}

WcException::~WcException(void) {};

std::string WcException::What(void) {
	return this->_message;
}

unsigned int WcException::Code(void) {
	return this->_code;
}

void WcException::PrintError(void) {
	printf("\033[1;31m[WcException] - Error: %s (%d)\033[0m\n", this->What().c_str(), this->Code());
}

void WcException::PrintError(std::string what, unsigned int code) {
	printf("\033[1;31m[WcException] - Error: %s (%d)\033[0m\n", what.c_str(), code);
}

#endif
